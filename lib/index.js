'use strict'

// The name of the package in order to give the Antora logger a useful name
const { name: packageName } = require('../package.json')
const tar = require('tar-stream');
const fs = require('fs');
const zlib = require('zlib');
const https = require('https')
var tempdir = './temp'

var _contentCatalog
var headversion

module.exports.register = function ({ config }) {

    const logger = this.require('@antora/logger').get(packageName)
    logger.info("registered")

    this.on('contentAggregated', function ({ contentAggregate }) {
        if (contentAggregate.length == 1) {
            headversion = contentAggregate[0].version
            logger.info("head version: " + headversion)
        }
    })

    this.on('contentClassified', async ({ contentCatalog }) => {
        _contentCatalog = contentCatalog
        logger.debug("running helpfile extension")

        var beforeInsert = ""
        var afterInsert = ""

        if (config.data.before_insert !== undefined) {
            beforeInsert = config.data.before_insert;
        }

        if (config.data.after_insert !== undefined) {
            afterInsert = config.data.after_insert;
        }

        for (const source of config.data.src) {
            logger.debug("downloading: " + source.url);
            const data = await downloadAndExtract(logger, source.url);

            for (var version of source.versions) {
                version = version.replace("HEAD", headversion)
                logger.info("adding helpfiles to " + version)
                for (var i = 1; i < data.length; i++) {
                    try {
                        addHelpFile(source.component, logger, data[i][0], beforeInsert + data[i][1].toString() + afterInsert, version);
                    } catch (error) {
                        logger.debug("skipping duplicate")
                    }
                }
            }
        }
    })
}

function addHelpFile(component, logger, filename, text, version) {
    logger.debug("adding: " + filename)
    _contentCatalog.addFile({
        contents: Buffer.from(text),
        path: 'modules/ROOT/examples/help/' + filename,
        src: {
            path: 'modules/ROOT/examples/help/' + filename,
            component: component,
            version: version,
            module: 'ROOT',
            family: 'example',
            relative: 'help/' + filename,
        },
    })
}

function downloadAndExtract(logger, url) {
    return new Promise(resolve => {

        const extract = tar.extract();

        var data = [[]];
        var currentFile = 0;
        var lastName = "";

        extract.on('entry', function (header, stream, cb) {
            stream.on('data', function (chunk) {
                var name = header.name.split('/')[1]

                if (lastName == name) {
                    data[currentFile][1] += chunk
                } else {
                    currentFile++;
                    lastName = name
                    data.push([name, chunk])
                }
            });

            stream.on('end', function () {
                cb();
            });

            stream.resume();
        });

        extract.on('finish', function () {
            logger.info(data.length.toString() + " helpfiles found")
            logger.debug("extraction done, deleting temp folder " + tempdir)
            fs.rmSync(tempdir, { recursive: true, force: true })
            resolve(data)
        });

        //Download
        //if (!fs.existsSync(tempdir)) {
            logger.debug("creating temp folder")
            tempdir = fs.mkdtempSync("./temp")
        //}
        logger.debug("Downloading into " + tempdir)
        const file = fs.createWriteStream(tempdir + '/download.tar.gz');

        https.get(url, function (response) {
            response.pipe(file);
            //after download completed close filestream
            file.on("finish", () => {
                file.close();
                logger.debug("Download Completed, extracting from " + tempdir)

                //extracting:
                fs.createReadStream(tempdir + '/download.tar.gz')
                    .pipe(zlib.createGunzip())
                    .pipe(extract);

                logger.debug("Readstream created")
            })
        });
    });
}
